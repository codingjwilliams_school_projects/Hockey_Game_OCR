import json
class InvalidPlayerException(Exception): pass
class InvalidTeamException(Exception): pass

class Player:
    def __init__(self, name, uid, team, attack, defence, validate=False):
        
        # Store all params in class
        self.name = name
        self.uid = uid
        self.team = team
        self.attack = attack
        self.defence = defence
        if validate:
            self.verify()
    def __str__(self):
        return self.name + " [" + str(self.attack) + "A " + str(self.defence) + "D]"
    def verify(self):
        # Check attack and defence are sensible
        if not ((0 <= self.attack) and (self.attack <= 10)): raise InvalidPlayerException("Player " + self.name + "[" + self.uid + "] attack not within constraints")
        if not ((0 <= self.defence) and (self.defence <= 7)): raise InvalidPlayerException("Player " + self.name + "[" + self.uid + "] defence not within constraints")
    def jsonify(self):
        return {"name": self.name, "uid": self.uid, "attack": self.attack, "defence": self.defence}
class Team:
    def __init__(self, _id, *players):
        self.players = list(players)
        self.id = _id
    @staticmethod
    def load(file, verify=True):
        with open(file) as f:
            teamRaw = json.loads(f.read())
        team = Team(teamRaw["id"])
        team.players = [Player(x["name"], x["uid"], team, x["attack"], x["defence"]) for x in teamRaw["players"]]
        if verify: team.verify()
        return team
        
    def calculate_keeper(self):
        return max(self.players, key=lambda player: player.defence)
    
    def verify(self):
        if len(self.players) != 6: raise InvalidTeamException("There must be exactly 6 players")
        total_attack = 0
        total_defence = 0
        for player in self.players:
            total_attack += player.attack
            total_defence += player.defence
        if total_attack != 35: raise InvalidTeamException("The total attack values for the team must be 35")
        if total_defence != 35: raise InvalidTeamException("The total defence values for the team must be 35")
        for player in self.players:
            try: player.verify()
            except Exception as e: raise e
        return True
    def jsonify(self):
        return {
            "id": self.id,
            "players": [p.jsonify() for p in self.players]
        }
    def __str__(self):
        return "Team '" + self.id + "': [" + ", ".join([str(p) for p in self.players]) + "]"
    def print_team(self):
        import sys
        e = None
        is_valid = False
        try:
            self.verify()
            is_valid = True
        except Exception as err:
            e = err
        max_len = 0
        for p in self.players:
            if len(p.name) > max_len: max_len = len(p.name) + 1
            
        sys.stdout.shell.write("Team " + self.id + " ", "stderr")
        if not is_valid:
            sys.stdout.shell.write("[INVALID] - " + str(e), "ERROR")
        else:
            sys.stdout.shell.write("[VALID]", "STRING")
        for p in self.players:
            print()
            sys.stdout.shell.write(" - " + p.name + (" " * (max_len - len(p.name))) + " [", "stdin")
            sys.stdout.shell.write('%02d' % p.attack + " Attack", "hit")
            sys.stdout.shell.write("|", "stdin")
            sys.stdout.shell.write(str(p.defence) + " Defence", "hit")
            sys.stdout.shell.write("]", "stdin")
        
class PlayingTeam(Team):
    def __init__(self, _id, game, *players):
        self.players = list(players)
        self.id = _id
        self.game = game
        
class Game:
    def __init__(self, team_a, team_b):
        # Setup variables
        self.team_a = team_a
        self.team_b = team_b
        self.team_a_penalties = 0
        self.team_b_penalties = 0
        
class PlayingPlayer(Player):
    def __init__(self, name, uid, team, attack, defence, game):
        # Set up other stuff
        super().__init__(self, name, uid, team, attack, defence)

        # Set up game
        self.game = game

    def play(self, goalkeeper):
        dif = self.attack - goalkeeper.defence
        if self.team.id == self.game.team_a.id: 
            # If we're on team A
            dif += self.game.team_a_penalties - self.game.team_b_penalties
        else:
            # If we're on team B
            dif += self.game.team_b_penalties - self.game.team_a_penalties
        if dif < 0:
            return goalkeeper
        else:
            return self
    
        
        
