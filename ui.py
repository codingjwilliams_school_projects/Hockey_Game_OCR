from classes import *
import game_mechanics
import sys
import team_management
def p_ask(player_num, message, nl=True):
    if player_num == 1:
        return sys.stdout.shell.write(message + ("\n" if nl else ""), "hit")
    if player_num == 2:
        return sys.stdout.shell.write(message + ("\n" if nl else ""), "ERROR")

first_loop = True


while True:
    if first_loop:
        print("\n" * 200)
        print("Welcome to the hockey game program! You should now designate a player " +
              "one and a player two. \nAll questions in the 'play' menu aimed directly at player one will be displayed" +
              " in ", end="")
        p_ask(1, "this font", False)
        print(" and any messages directed towards player two will be displayed in ", end="")
        p_ask(2, "this font.", False)
        print()
    else: print("\n" * 200)
    choice = input("\nWhat would you like to do?\n - Press 1 for team management\n - Press 2 to play\n\n> ")
    if choice == "1":
        team_management.manage_ui()
    if choice == "2":
        game_mechanics.play()
        
