from classes import *
import game_mechanics
import sys
import os
import team_management
import time
def p_ask(player_num, message, nl=True):
    if player_num == 1:
        return sys.stdout.shell.write(message + ("\n" if nl else ""), "hit")
    if player_num == 2:
        return sys.stdout.shell.write(message + ("\n" if nl else ""), "ERROR")
def valid(t):
    try: t.verify()
    except: return False
    return True
get_all_except = (lambda players, player: [p for p in players if p.uid != player.uid])
def play():
    print("\n" * 200)
    teams_files = os.walk("teams")
    teams = []
    for root, dirs, files in teams_files:
        for filename in files:
            fn = os.path.join(".", "teams", filename)
            with open(fn) as f:
                teamRaw = json.loads(f.read())
                team = Team(teamRaw["id"])
                team.filename = fn
                team.players = [Player(x["name"], x["uid"], team, x["attack"], x["defence"]) for x in teamRaw["players"]]
                teams += [team]
    teams = [t for t in teams if valid(t)]
    if len(teams) < 2:
        input("There are less than 2 valid teams. Please add teams and try again.")
        return
    print("Available teams (invalid teams not shown):")
    for i, team in enumerate(teams):
        sys.stdout.shell.write(str(i + 1), "hit")
        sys.stdout.shell.write(". ", "stdin")
        team.print_team()
        print()
    print("\n" * 3)
    p_ask(1, "Player 1, please choose a team: ", nl=False)
    p1_team = teams[int(input()) - 1]
    p_ask(2, "Player 2, please choose a team: ", nl=False)
    p2_team = teams[int(input()) - 1]

    p1_goalkeeper = p1_team.calculate_keeper()
    p2_goalkeeper = p2_team.calculate_keeper()
    p1_attackers = get_all_except(p1_team.players, p1_goalkeeper)
    p2_attackers = get_all_except(p2_team.players, p2_goalkeeper)
    p1_goals, p2_goals = 0, 0
    print("\n" * 200)
    for ind, player in enumerate(p1_attackers):
        for team_num in range(1,3):
            
            if team_num == 1:
                print("Player 1 keeper: ")
                p = p1_goalkeeper
                sys.stdout.shell.write(" " + p.name + "  [", "stdin")
                sys.stdout.shell.write('%02d' % p.attack + " Attack", "hit")
                sys.stdout.shell.write("|", "stdin")
                sys.stdout.shell.write(str(p.defence) + " Defence", "hit")
                sys.stdout.shell.write("]\n", "stdin")
                print("Player 2 keeper: ")
                p = p2_goalkeeper
                sys.stdout.shell.write(" " + p.name + "  [", "stdin")
                sys.stdout.shell.write('%02d' % p.attack + " Attack", "hit")
                sys.stdout.shell.write("|", "stdin")
                sys.stdout.shell.write(str(p.defence) + " Defence", "hit")
                sys.stdout.shell.write("]\n", "stdin")
                print("\nPlayer 1 remaining attackers:")
                for i, p in [[e[0] + 1, e[1]] for e in enumerate(p1_attackers)]:
                    print()
                    sys.stdout.shell.write( str(i) + " - " + p.name + "  [", "stdin")
                    sys.stdout.shell.write('%02d' % p.attack + " Attack", "hit")
                    sys.stdout.shell.write("|", "stdin")
                    sys.stdout.shell.write(str(p.defence) + " Defence", "hit")
                    sys.stdout.shell.write("]", "stdin")
                print("\n")
                p_ask(1, "Player 1, please choose an attacker: ", nl=False)
                n = int(input()) - 1
                p1_currentattacker = p1_attackers[n]
                del p1_attackers[n]
                print("\n" * 200)
                print("P1 attacker: ")
                p = p1_currentattacker
                sys.stdout.shell.write(" " + p.name + "  [", "stdin")
                sys.stdout.shell.write('%02d' % p.attack + " Attack", "hit")
                sys.stdout.shell.write("|", "stdin")
                sys.stdout.shell.write(str(p.defence) + " Defence", "hit")
                sys.stdout.shell.write("]\n", "stdin")
                print("vs P2 goalkeeper: ")
                p = p2_goalkeeper
                sys.stdout.shell.write(" " + p.name + "  [", "stdin")
                sys.stdout.shell.write('%02d' % p.attack + " Attack", "hit")
                sys.stdout.shell.write("|", "stdin")
                sys.stdout.shell.write(str(p.defence) + " Defence", "hit")
                sys.stdout.shell.write("]\n", "stdin")
                print("\n\nPlaying...")
                with open("resources\\asciiart_hockey.txt") as f: print(f.read())
                time.sleep(3)
                dif = p1_currentattacker.attack - p2_goalkeeper.defence
                dif += p1_goals - p2_goals
                print("\n" * 200)
                if dif >= 0:
                    p1_goals += 1
                    print("Player 1 won that round! Score: (P1-P2) " + str(p1_goals) + "-" + str(p2_goals))
                else:
                    p2_goals += 1
                    print("Player 2 won that round! Score: (P1-P2) " + str(p1_goals) + "-" + str(p2_goals))
                print("\n" * 5)
            else:
                print("Player 1 keeper: ")
                p = p1_goalkeeper
                sys.stdout.shell.write(" " + p.name + "  [", "stdin")
                sys.stdout.shell.write('%02d' % p.attack + " Attack", "hit")
                sys.stdout.shell.write("|", "stdin")
                sys.stdout.shell.write(str(p.defence) + " Defence", "hit")
                sys.stdout.shell.write("]\n", "stdin")
                print("Player 2 keeper: ")
                p = p2_goalkeeper
                sys.stdout.shell.write(" " + p.name + "  [", "stdin")
                sys.stdout.shell.write('%02d' % p.attack + " Attack", "hit")
                sys.stdout.shell.write("|", "stdin")
                sys.stdout.shell.write(str(p.defence) + " Defence", "hit")
                sys.stdout.shell.write("]\n", "stdin")
                print("\nPlayer 2 remaining attackers:")
                for i, p in [[e[0] + 1, e[1]] for e in enumerate(p2_attackers)]:
                    print()
                    sys.stdout.shell.write( str(i) + " - " + p.name + "  [", "stdin")
                    sys.stdout.shell.write('%02d' % p.attack + " Attack", "hit")
                    sys.stdout.shell.write("|", "stdin")
                    sys.stdout.shell.write(str(p.defence) + " Defence", "hit")
                    sys.stdout.shell.write("]", "stdin")
                print("\n")
                p_ask(2, "Player 2, please choose an attacker: ", nl=False)
                n = int(input()) - 1
                p2_currentattacker = p2_attackers[n]
                del p2_attackers[n]
                print("\n" * 200)
                print("P2 attacker: ")
                p = p2_currentattacker
                sys.stdout.shell.write(" " + p.name + "  [", "stdin")
                sys.stdout.shell.write('%02d' % p.attack + " Attack", "hit")
                sys.stdout.shell.write("|", "stdin")
                sys.stdout.shell.write(str(p.defence) + " Defence", "hit")
                sys.stdout.shell.write("]\n", "stdin")
                print("vs P1 goalkeeper: ")
                p = p1_goalkeeper
                sys.stdout.shell.write(" " + p.name + "  [", "stdin")
                sys.stdout.shell.write('%02d' % p.attack + " Attack", "hit")
                sys.stdout.shell.write("|", "stdin")
                sys.stdout.shell.write(str(p.defence) + " Defence", "hit")
                sys.stdout.shell.write("]\n", "stdin")
                print("\n\nPlaying...")
                with open("resources\\asciiart_hockey.txt") as f: print(f.read())
                time.sleep(3)
                dif = p2_currentattacker.attack - p1_goalkeeper.defence
                dif += p2_goals - p1_goals
                print("\n" * 200)
                if dif >= 0:
                    p2_goals += 1
                    print("Player 2 won that round! Score: (P1-P2) " + str(p1_goals) + "-" + str(p2_goals))
                else:
                    p1_goals += 1
                    print("Player 1 won that round! Score: (P1-P2) " + str(p1_goals) + "-" + str(p2_goals))
                print("\n" * 5)
    input("Game over!")
