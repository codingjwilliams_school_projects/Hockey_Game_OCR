# Scenario 
Sophie is designing a hockey game for two users. Each turn, the users try to score a penalty against each other. The game has a number of teams and each team has a unique name. The teams are stored in an external text file. Each team consists of six players. The game allows each user to choose one team to play with from the teams stored in the text file. Players may not choose the same team. Players may create their own team. 

Each team player has three attributes:
 - Name
 - Attack (a number between 0 and 10 inclusive) 
 - Defence (a number between 0 and 7 inclusive). 
The total of the Attack and Defence values for the whole team must equal 35. For example, a team of six players with an Attack of 10, and Defence of 0 would break the rules, as the total would be 60. 

To play the game, the player with the highest defence in the team is selected as the Goalkeeper for that team, and the other team members become the Attackers for that team. User One then selects an Attacker to take a penalty. Each Attacker can only take one penalty.  
To score a goal, the following process is used: 
 - calculate the difference between the Attack of the player taking the penalty and the Defence of the goalkeeper
 - add the difference between the penalties scored by the attacking player's team and the penalties scored by the defending player's team. 
 - If the result is negative then the penalty is saved, otherwise it is scored. 

Once User One has taken a penalty, User Two then chooses a player to take a penalty, and so on, until all five Attackers have taken penalties. The team with the most goals after all five penalties are taken are the winners. 
If both teams score the same number of goals, Overtime mode is entered. During Overtime, each Attacker is allowed to take one more penalty. The winner of Overtime is the first team to score more goals than the other team after the same number of penalties have been taken by both teams. If the teams are still even after Overtime, then the result is a draw. 

Analyse the requirements for this system and design, develop, test and evaluate a program that: 
1. allows a user to create and store the details of a team\*. 
2. allows the two users to play the game as described above. 
3. allows the user to update the Name, Attack and Defence value of each player in a team. 
4. adds the result of each game to an external file. This must contain the two team names, the result (Win, Draw, Loss) and a final score. 
5. allows the user to display and save a report that asks the user to input two different team names and outputs the result of every game that has been played between the two teams. If the teams have not played each other, a suitable message is displayed. 
6. allows the user to display and save a report that displays two leader boards. One leader board will show the top three teams sorted by number of games won. The second leader board will show the top five teams sorted by the fewest number of goals conceded. 

**NOTE TO CANDIDATES:**
 - You should test your game with a minimum of 4 teams. 

J276/03 Jun18 - Copyright OCR 2018
