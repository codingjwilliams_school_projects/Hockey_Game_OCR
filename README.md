# OCR Hockey Game

![OCR Logo](http://www.baytreecentre.org/wp-content/uploads/2015/12/ocr-logo.png)

# Planning:

# Team

### Data Structure
	Team [ 
		Player {...}, 
		Player {...} 
	] id=int

### JSON Representation
	{
		"

## Methods:

### Verify
	set total_attack 0
	set total_defence 0
	loop team members
		set total_attack to total_attack plus the member's attack
		set total_defence to total_defence plus the member's defence
	output true if total_attack and total_defence are 35 exactly
# Player

### Data Structure
	Player {
		“name”: str,
		"id": int,
		"team": Team,
		“attack”: 0 <= int <= 10,
		“defence”: 0 <= int <= 7
	}
## Methods
### Play
	input goalkeeper
	set dif to player.attack - goalkeeper.defence
	add (player.team.penalties_scored - goalkeeper.team.penalties_scored) to dif
	if dif negative
		output goalkeeper
	if dif positive
		output player

---


	class InvalidPlayerException(Exception): pass

	class Player:
	    def __init__(self, name, uid, team, attack, defence):
	        # Check attack and defence are sensible
	        if not (0 <= attack) and (attack <= 10): raise InvalidPlayerException("Attack not within constraints")
	        if not (0 <= defence) and (defence <= 10): raise InvalidPlayerException("Defence not within constraints")

	        # Store all params in class
	        self.name = name
	        self.uid = uid
	        self.team = team
	        self.attack = attack
	        self.defence = defence
	    def __str__(self):
	        return self.name + " [" + str(self.attack) + "A " + str(self.defence) + "D]"
	class PlayingPlayer(Player):
	    def __init__(self, name, uid, team, attack, defence, game):
	        # Set up other stuff
	        super().__init__(self, name, uid, team, attack, defence)

	    def play(self, goalkeeper):
	        
	        dif = self.attack - goalkeeper.defence

