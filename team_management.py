from classes import *
import os # I'm not going against the rules, dw miss sadler ^.^
import time # just to get the current epoch
import sys # colors :D
import json # team file handling
import uuid # ids
import random

def manage_ui():
    while True:
        print("\n" * 100)
        print("Team management UI\n")
        teams_files = os.walk("teams")
        teams = []
        for root, dirs, files in teams_files:
            for filename in files:
                fn = os.path.join(".", "teams", filename)
                with open(fn) as f:
                    teamRaw = json.loads(f.read())
                    team = Team(teamRaw["id"])
                    team.filename = fn
                    team.players = [Player(x["name"], x["uid"], team, x["attack"], x["defence"]) for x in teamRaw["players"]]
                    teams += [team]
        # We now have a team object with all teams from folder
        print("Available teams:")
        for i, team in enumerate(teams):
            sys.stdout.shell.write(str(i + 1), "hit")
            sys.stdout.shell.write(". ", "stdin")
            team.print_team()
            print()
        print("\n\n")
        print("Do you want to")
        print(" - Edit a team? Type ", end='')
        sys.stdout.shell.write("edit <team number>", "hit")
        print()
        print(" - Create a team? Type ", end='')
        sys.stdout.shell.write("create", "hit")
        print()
        print(" - Delete a team? Type ", end='')
        sys.stdout.shell.write("delete <team number>", "hit")
        print()
        print(" - Exit? Type ", end='')
        sys.stdout.shell.write("exit", "hit")
        print()
        option = input()
        if option.startswith("edit"):
            tn = int(option.split(" ")[1]) - 1
            try:
                team_to_edit = teams[tn]
            except IndexError:
                print("\n" * 100)
                input("Not a valid team [enter]")
                continue
            new_team = team_editor(team_to_edit)
            with open(new_team.filename, mode="w") as f:
                f.write(json.dumps(new_team.jsonify(), indent=4))
        if option.startswith("delete"):
            tn = int(option.split(" ")[1]) - 1
            try:
                t = teams[tn]
            except IndexError:
                print("\n" * 100)
                input("Not a valid team [enter]")
                continue
            os.remove(t.filename)
            del teams[tn]

        if option == "create":
            new_team = create_team()
            with open("teams/" + str(uuid.uuid4()) + ".json", "w") as f:
                f.write(json.dumps(new_team.jsonify(), indent=4))
        if option == "exit":
            return

def create_team():
    print("\n" * 100)
    shouldBeRandom = input("Would you like the team to be filled with random values (y/n)?\n")
    
    t = Team(str(uuid.uuid4()))
    with open("resources\\playernames.txt") as f: names = f.read().split("\n")
    t.players = [Player(random.choice(names), str(uuid.uuid4()), t, 0, 0) for i in range(6)]
    t = team_editor(t)
    return t

def team_editor(team):
    while True:
        print(("\n" * 100) + "Team editor\n\nTeam to edit:\n")
        team.print_team()
        print("\n\n")
        print("What would you like to edit?")
        print(" - Press 1 to edit individual player stats")
        print(" - Press 2 to edit team id (Note: WILL REMOVE ALL HISTORY FOR THIS TEAM)")
        print(" - Press 3 to exit team editor")
        choice = input()
        if choice == "1":
            print(("\n" * 100) + "Which player would you like to edit?")
            for i, p in enumerate(team.players): print(str(i+1) + " - " + p.name)
            playernum = int(input()) - 1
            team.players[playernum] = player_editor(team.players[playernum])
        if choice == "2":
            print("\n" * 100)
            print("What should the new id be?")
            team.id = input()
        if choice == "3":
            return team
def player_editor(p):
    while True:
        print(("\n" * 100) + "Player editor: ")
        sys.stdout.shell.write(p.name + (" " * 4) + " [", "stdin")
        sys.stdout.shell.write('%02d' % p.attack + " Attack", "hit")
        sys.stdout.shell.write("|", "stdin")
        sys.stdout.shell.write(str(p.defence) + " Defence", "hit")
        sys.stdout.shell.write("]", "stdin")

        print("\n\n\n")
        print("What would you like to edit?")
        print(" - Press 1 to edit name")
        print(" - Press 2 to edit attack")
        print(" - Press 3 to edit defence")
        print(" - Press 4 to exit player editor")
        choice = input()
        if choice == "1":
            p.name = input("New name > ")
        if choice == "2":
            p.attack = int(input("Attack > "))
        if choice == "3":
            p.defence = int(input("Defence > "))
        if choice == "4":
            break
    return p
        
